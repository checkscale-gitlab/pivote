# pivote

Proyecto de imagen SO para ser usado como proxy administrativo de servicios cloud

# Versionado

Algo común es realizar el manejo de versiones mediante 3 números: X.Y.Z y cada uno indica una cosa diferente:
* El primero (X) se le conoce como versión mayor y nos indica la versión principal del software. Ejemplo: 1.0.0, 3.0.0
* El segundo (Y) se le conoce como versión menor y nos indica nuevas funcionalidades. Ejemplo: 1.2.0, 3.3.0
* El tercero (Z) se le conoce como revisión y nos indica que se hizo una revisión del código por algun fallo. Ejemplo: 1.2.2, 3.3.4

Ahora que conocemos el significado de cada número, viene una pregunta importante: ¿cómo sabemos cuando cambiarlos y cuál cambiar?
* Versión mayor o X, cuando agreguemos nuevas funcionalidades importantes, puede ser como un nuevo modulo o característica clave para la funcionalidad.
* Versión menor o Y, cuando hacemos correcciones menores, cuando arreglamos un error y se agregan funcionalidades que no son cruciales para el proyecto.
* Revisión o Z, cada vez que entregamos el proyecto.

# COMANDOS
* docker build -t pivote:0.0.5 .
* docker run -d -p 2222:22 pivote:0.0.5
* docker exec -ti 15c7e414fb9d /bin/bash 

* ssh localhost -p 2222 -l sgarces