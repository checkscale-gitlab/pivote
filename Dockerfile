FROM archlinux
# Seleccion de directorio
WORKDIR /opt
# Instalacion desde repo
RUN patched_glibc=glibc-linux4-2.33-4-x86_64.pkg.tar.zst && \
    curl -LO "https://repo.archlinuxcn.org/x86_64/$patched_glibc" && \
    bsdtar -C / -xvf "$patched_glibc"
RUN pacman -Syu --noconfirm
RUN pacman -S vim openssh k9s python --noconfirm
# Instalacion desde fuente
RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
RUN install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
RUN curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-367.0.0-linux-x86.tar.gz
RUN tar -zxvf google-cloud-sdk-367.0.0-linux-x86.tar.gz
RUN ./google-cloud-sdk/install.sh --usage-reporting false --rc-path /etc/bash.bashrc --command-completion true --path-update true
RUN ln -s /opt/google-cloud-sdk/bin/gcloud /bin/gcloud
# Configuraciones de SSH
RUN  /usr/bin/ssh-keygen -A
RUN sed -i -e 's/^UsePAM yes/UsePAM no/g' /etc/ssh/sshd_config
RUN sed -i -e 's/^\#Banner none/Banner \/etc\/ssh\/message/g' /etc/ssh/sshd_config
COPY message /etc/ssh/
# Expose tcp port
EXPOSE	 22
# Creacion de usuarios
RUN useradd -m -s /bin/bash admin
RUN echo 'admin:temporal' | chpasswd
RUN sed -i -e '$aAllowUsers admin' /etc/ssh/sshd_config
# Instalar helm
RUN pacman -S helm --noconfirm
# Start ssh
CMD	["/usr/sbin/sshd", "-D"]